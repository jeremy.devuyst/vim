"setting preferences
set nocompatible
filetype on
filetype plugin on
syntax on
set confirm		" asks to save if trying to quit or leave buffer without saving
" visual
    set background=dark
    set number
    set showcmd
    set showmatch          " Show matching brackets.
    set termguicolors
" search
    set ignorecase         " Do case insensitive matching
    set smartcase          " Do smart case matching (match lower with upper but not the other way around
    set incsearch          " Incremental search 
    set hlsearch		" Highlight search
" tabs & indent
    set shiftwidth=4 	" tabspace
    set tabstop=4
    set softtabstop=4 	"if backspace on tab don't go back one space
    set expandtab 		"tabs dont become spaces
    set autoindent 	" indents after curly brackets and keywords
    set smartindent 	" indents if above indented carry's over comment sign
" folding
    set foldmethod=indent "create folds at indents
    set foldcolumn=0	" dont want a column showing folds
    set foldlevelstart=0	"opening a file has this level of folds open, the rest closed
    set foldnestmax=4	"max number of folds created
    set splitbelow "when splitting a window, new window is below
    set splitright "when splitting a window, new window is right
    set tags=tags;~ "look for tags file until reach home
    set wildmode=longest,list,full	"when in command mode, tab autocompletes like bash
    set wildmenu "comes with above
" autocommands
    "don't add comments after adding commented line
    autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o 
    " When editing a file, always jump to the last known cursor position (if valid).
    autocmd BufReadPost *
      \ if line("'\"") >= 1 && line("'\"") <= line("$") && &ft !~# 'commit'
      \ |   exe "normal! g`\""
      \ | endif
