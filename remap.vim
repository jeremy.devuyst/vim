"inoremap for insert mode, o for object (y...,d...,), v for visual, c for command, n for normal
"esc faster!
	inoremap kj <esc> 
	onoremap kj <esc> 
	vnoremap kj <esc> 
	cnoremap kj <esc> 
"save faster!
    nnoremap <Tab> :w <cr>
"save with sudo 
    cmap w!! w !sudo tee %
"reduce shift key pressing!
    "nnoremap ; :
"copy to clipboard
    vnoremap <C-C> :!xclip -sel c <CR>u
"switch numbers with whats on top!
	onoremap ! 1
	onoremap @ 2
	onoremap # 3
	onoremap $ 4
	onoremap % 5
	onoremap ^ 6
	onoremap & 7
	onoremap * 8
	onoremap ( 9
	onoremap ) 0
	onoremap 1 !
	onoremap 2 @
	onoremap 3 #
	onoremap 4 $
	onoremap 5 %
	onoremap 6 ^
	onoremap 7 &
	onoremap 8 *
	onoremap 9 (
	onoremap 0 )
	vnoremap ! 1
	vnoremap @ 2
	vnoremap # 3
	vnoremap $ 4
	vnoremap % 5
	vnoremap ^ 6
	vnoremap & 7
	vnoremap * 8
	vnoremap ( 9
	vnoremap ) 0
	vnoremap 1 !
	vnoremap 2 @
	vnoremap 3 #
	vnoremap 4 $
	vnoremap 5 %
	vnoremap 6 ^
	vnoremap 7 &
	vnoremap 8 *
	vnoremap 9 (
	vnoremap 0 )
	nnoremap ! 1
	nnoremap @ 2
	nnoremap # 3
	nnoremap $ 4
	nnoremap % 5
	nnoremap ^ 6
	nnoremap & 7
	nnoremap * 8
	nnoremap ( 9
	nnoremap ) 0
	nnoremap 1 !
	nnoremap 2 @
	nnoremap 3 #
	nnoremap 4 $
	nnoremap 5 %
	nnoremap 6 ^
	nnoremap 7 &
	nnoremap 8 *
	nnoremap 9 (
	nnoremap 0 )
"enter and backspace in normal
	nnoremap <Return> o<esc>
	nnoremap <Backspace> i<backspace><esc>l
"commands with spacebar as leader
	" make spacebar the leader
	let mapleader=" "
    "quit faster
	noremap <Leader>q :q <cr>
    "split screen
    noremap <Leader>w :vsp <cr>
	"switch split screen easier
	noremap <Leader>j <C-W>j
	noremap <Leader>k <C-W>k
	noremap <Leader>h <C-W>h
	noremap <Leader>l <C-W>l
	"toggle folds
	nnoremap <Leader>a za
	nnoremap <Leader><Leader>a z<S-A>
	" toggle search highlighting
	nnoremap <Leader><Space> :set hlsearch!<cr>
	"end of file
	nnoremap <Leader>g <S-G>
	" search and replace
	nnoremap <Leader>r :%s/
	"when using tags or finding compiler errors, or grep can be useful
	nnoremap <Leader>n :cn <CR>
	"jump to file through ctags
	nnoremap <Leader>] <C-]>
	"come back from tag jump
	nnoremap <Leader>t <C-T>
    "come back from file jump
    nnoremap <leader>o <C-O>
    "move faster than k or j
    nnoremap <leader>d <C-D>
    nnoremap <leader>u <C-U>
    "indent
    nnoremap <Leader><Tab> >>
	nnoremap <Leader><Leader><Tab> <<
	vnoremap <Leader><Tab> >
	vnoremap <Leader><Leader><Tab> <
    "v-block
    nnoremap <Leader>v <C-V>
"quick bash bang
    command Bash :normal! i#!/bin/bash
"fold all
    command Unfold :normal! zR
    command Fold :normal! zM
" sudo bang 
    command Sudo :normal! :w !sudo tee %
"remove lines only containing whitespace
    command EraseEmptyLines :g/^\s*$/d
