" save then run current file with no arguments
noremap <F2> :w <CR> :! clear && python % <CR>
inoremap <F2> <esc>:w <CR> :! clear && python % <CR>
" save then run current file with arguments
noremap <F3> :w <CR> :! clear && python %
inoremap <F3> <esc>:w <CR> :! clear && python %
"run in interactive mode
noremap <F4> :w <CR> :! python -i %<CR>
inoremap <F4> <esc> :w <CR> :! python -i %<CR> 

let python_highlight_all=1 "its effect outlined in python.vim
